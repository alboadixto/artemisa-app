import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

export interface User {
  id?: string;
  name: string;
  email: string;
  password: string;
  canal_venta: string;
}

@Injectable({
  providedIn: "root",
})
export class UserService {
  API = "http://lumsacdev.entercode.cl/api/users";

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(this.API);
  }

  createUser(
    name: string,
    email: string,
    password: string,
    canal_venta: string
  ) {
    return this.http.post(this.API, { name, email, password, canal_venta });
  }

  deleteUser(id: string) {
    return this.http.delete(`${this.API}/${id}`);
  }

  getUserById(id: string) {
    return this.http.get<User>(`${this.API}/${id}`);
  }

  updateUser(id: string, user: User) {
    //console.log(user);
    return this.http.put(`${this.API}/${id}`, user);
  }
}
