import { UserService, User } from "./../services/user.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { LoadingController } from "@ionic/angular";

@Component({
  selector: "app-form-user",
  templateUrl: "./form-user.page.html",
  styleUrls: ["./form-user.page.scss"],
})
export class FormUserPage implements OnInit {
  userId: string;

  editing = false;

  user: User = {
    name: "",
    email: "",
    password: "",
    canal_venta: "",
  };

  constructor(
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public loadingCtrl: LoadingController
  ) {}

  hideLoading() {
    this.loadingCtrl.dismiss();
  }

  showLoading() {
    this.loadingCtrl
      .create({
        message: "Cargando.....",
      })
      .then((loading) => {
        loading.present();
        {
        }
      });
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      this.userId = paramMap.get("userId");

      if (this.userId) {
        this.showLoading();

        this.editing = true;

        this.userService.getUserById(this.userId).subscribe((res) => {
          this.user = res;

          this.hideLoading();
        });
      }
    });
  }

  saveUser() {
    this.showLoading();

    this.userService
      .createUser(
        this.user.name,
        this.user.email,
        this.user.password,
        this.user.canal_venta
      )
      .subscribe(
        (res) => {
          console.log(res),
            this.router.navigate(["/users/Usuarios"]),
            this.hideLoading();
        },
        (err) => console.error(err)
      );
  }

  updateUser() {
    this.showLoading();

    this.userService
      .updateUser(this.user.id, {
        name: this.user.name,
        email: this.user.email,
        password: this.user.password,
        canal_venta: this.user.canal_venta,
      })
      .subscribe(
        (res) => {
          console.log(res),
            this.router.navigate(["/users/Usuarios"]),
            this.hideLoading();
        },
        (err) => console.error(err)
      );
  }
}
