import { Component, OnInit } from "@angular/core";
import { UserService } from "../services/user.service";
import { ActivatedRoute } from "@angular/router";
import { AlertController, LoadingController } from "@ionic/angular";

@Component({
  selector: "app-users",
  templateUrl: "./users.page.html",
  styleUrls: ["./users.page.scss"],
})
export class UsersPage implements OnInit {
  public users: string;

  usuarios: any = [];

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    public loadingCtrl: LoadingController
  ) {}

  hideLoading() {
    this.loadingCtrl.dismiss();
  }

  showLoading() {
    this.loadingCtrl
      .create({
        message: "Cargando.....",
      })
      .then((loading) => {
        loading.present();
        {
        }
      });
  }

  loadUsers() {
    this.showLoading();

    this.users = this.activatedRoute.snapshot.paramMap.get("id");

    this.userService.getUsers().subscribe(
      (res) => {
        this.usuarios = res;

        this.hideLoading();
      },
      (err) => console.log(err)
    );
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.loadUsers();
  }

  async deleteUser(id) {
    const alert = await this.alertController.create({
      header: "Eliminar al usuario",
      //subHeader: "Eliminar al usuario",
      message: "Está seguro?",
      buttons: [
        {
          text: "SI",
          handler: () => {
            this.userService.deleteUser(id).subscribe(
              (res) => {
                console.log(res), this.loadUsers();
              },
              (err) => console.log(err)
            );
          },
        },
        "Cancelar",
      ],
    });

    await alert.present();
  }
}
